package acmv.jtools;

import java.util.logging.Logger;

public class Log {

	static final Logger log = Logger.getLogger("Logging");

	public static void error(Throwable throwable) {
		StackTraceElement[] st = throwable.getStackTrace();
		log.throwing(st[0].getMethodName(),st[0].getClassName(), throwable);
	}
	
}
