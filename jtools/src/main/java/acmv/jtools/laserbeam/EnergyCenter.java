package acmv.jtools.laserbeam;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import acmv.jtools.App;
import acmv.jtools.Log;

public class EnergyCenter implements Runnable {

	private String src;
	private File frameDir;
	private JFrame view;
	private ImageIcon icon;
	private JLabel label;
	private JLabel labelCrop;
	private VideoCapture camera;
	private BufferedImage image;
	private BufferedImage crop;
	private Size cropSize;
	private Mat frame;
	private Mat hsv;
	private Mat mask;
	private Mat hierarchy;
	private int threshold = 120;
	private int currentFrame;
	private int blurSize = 3;
	protected static final int FILTER_GAUSS = 0;
	protected static final int FILTER_MEDIAN = 1;
	protected int filterMethod = FILTER_GAUSS;
	private ArrayList<MatOfPoint> contours;
	private static final Scalar markerColor = new Scalar(0,0,255);

	abstract class SliderListener {

		public abstract void change(int value);
		
	}
	
	public EnergyCenter(String src) {
		this.src = src;
	}

	@Override
	public void run() {
		
		BufferedImage im = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR);
		
		camera = new VideoCapture(src);
		
		image = new BufferedImage((int)camera.get(App.CAP_PROP_FRAME_WIDTH), (int)camera.get(App.CAP_PROP_FRAME_HEIGHT), BufferedImage.TYPE_3BYTE_BGR);
		crop = new BufferedImage(200,200, BufferedImage.TYPE_3BYTE_BGR);
				
		view = new JFrame();
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.setSize(900, 600);
		Container cnt = view.getContentPane();
		cnt.setLayout(new BorderLayout());
		icon = new ImageIcon(image);
		label = new JLabel(new ImageIcon(image));
		JScrollPane scroller = new JScrollPane(label, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		cnt.add(scroller, BorderLayout.CENTER);

		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());
		labelCrop = new JLabel(new ImageIcon(crop));
		scroller = new JScrollPane(labelCrop, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroller.setMinimumSize(new Dimension(200, 200));
		rightPanel.add(scroller, BorderLayout.CENTER);

		JPanel rightActions = new JPanel();
		rightActions.setAlignmentX(Container.LEFT_ALIGNMENT);
		rightActions.setLayout(new BoxLayout(rightActions, BoxLayout.PAGE_AXIS));
		rightPanel.add(rightActions, BorderLayout.SOUTH);
		
		rightActions.add(newSlider(
		    	"Frame",
		    	80,
		    	(int)camera.get(App.CAP_PROP_FRAME_COUNT) - 1,
		    	0,
		    	new SliderListener(){
					@Override
					public void change(int value) {
						if(currentFrame == value){
							return;
						}
						showFrame(value);
					}
		    	}
		    ).getParent());
	    
		JSlider slider = newSlider(
		    	"Blur size",
		    	80,
		    	15,
		    	blurSize,
		    	new SliderListener(){
					@Override
					public void change(int value) {
						blurSize = value;
						if(blurSize % 2 != 1){
							blurSize++;
						}
						showFrame(currentFrame);
					}
		    	}
		    );
		
		rightActions.add(slider.getParent());
		
		JPanel filterChooseCnt = new JPanel();
		filterChooseCnt.setLayout(new FlowLayout());
		rightActions.add(filterChooseCnt);
		ButtonGroup group = new ButtonGroup();
		JRadioButton smallButton = new JRadioButton("Median", false);
		smallButton.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					filterMethod = FILTER_MEDIAN;
					showFrame(currentFrame);
				}
				
			}
			
		});
		group.add(smallButton);
		filterChooseCnt.add(smallButton);
		smallButton = new JRadioButton("Gauss", true);
		group.add(smallButton);
		smallButton.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					filterMethod = FILTER_GAUSS;
					showFrame(currentFrame);
				}
				
			}
			
		});
		
		filterChooseCnt.add(smallButton);
		
		rightActions.add(newSlider(
		    	"Threshold",
		    	80,
		    	255,
		    	threshold,
		    	new SliderListener(){
					@Override
					public void change(int value) {
						threshold = value;
						showFrame(currentFrame);
					}
		    	}
		    ).getParent());
		
		JToolBar toolBar = new JToolBar("Still draggable");
		
		JButton compileBtn = new JButton("Compile data");
		toolBar.add(compileBtn);
		compileBtn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				compile();
			}
			
		});
		
		rightPanel.add(toolBar, BorderLayout.PAGE_START);
		
		cnt.add(rightPanel, BorderLayout.EAST);
		
		
		view.setVisible(true);
		cropSize = new Size(crop.getWidth(), crop.getHeight());
		
    	frame = new Mat();
        hsv = new Mat();
        mask = new Mat();
        hierarchy = new Mat();

        EventQueue.invokeLater(new Runnable(){

			@Override
			public void run() {
				showFrame(0);
			}
        	
        });
        
        /*
        mask.release();
        frame.release();
        hsv.release();
        hierarchy.release();
        */
	}

	private JSlider newSlider(String label, int width, int maxValue, int value, final SliderListener listener) {
		
		final NumberFormat numberFormat = NumberFormat.getNumberInstance();
		
		final JSlider slider = new JSlider();
		slider.setMaximum(maxValue);
		slider.setValue(value);
	    JPanel panel = new JPanel();
	    final JTextField textField = new JFormattedTextField(numberFormat);
	    textField.setText(slider.getValue() + "");
	    textField.setColumns(3);
	    textField.setHorizontalAlignment(JTextField.CENTER);
	    textField.addFocusListener(new FocusListener(){

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void focusLost(FocusEvent e) {
				EventQueue.invokeLater(new Runnable(){
					@Override
					public void run() {
						try {
							int value = Math.min(numberFormat.parse(textField.getText()).intValue(), slider.getMaximum());
							//textField.setText(value + "");
							slider.setValue(value);
							ChangeEvent ce = new ChangeEvent(slider);
						    for(ChangeListener cl : slider.getChangeListeners()){
						        cl.stateChanged(ce);
						    }
						} catch (ParseException e) {
							Log.error(e);
						}
					}
				});

			}
	    	
	    });
	    textField.addActionListener( new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				slider.requestFocusInWindow();
			}} );
	    
		slider.addChangeListener(new ChangeListener(){
			@Override
			public void stateChanged(ChangeEvent e) {
				textField.setText(slider.getValue() + "");
				listener.change(slider.getValue());
			}
		});
		
		JLabel jlabel = new JLabel(label);
		Dimension jlabelSize = new Dimension(width, (int)jlabel.getPreferredSize().getHeight());
		jlabel.setMinimumSize(jlabelSize);
		jlabel.setPreferredSize(jlabelSize);
		jlabel.setMaximumSize(jlabelSize);
		
	    panel.add(jlabel);
	    panel.add(slider);
	    panel.add(textField, BorderLayout.SOUTH);

	    return slider;
	}
	
	protected void showFrame(int ind) {
   		
		currentFrame = ind;
		
		if(camera.set(App.CAP_PROP_POS_FRAMES, ind) && camera.read(frame)){
			proceedFrame(true, false, null, null);
		}

		
	}

	protected void compile() {
		
		camera.set(App.CAP_PROP_POS_FRAMES, 0);
		
		File dir = getOutputFrameDir();
		
		PrintWriter writer = null;
		try {
			File file = new File(dir, "data.csv");
			if(file.exists()){
				file.delete();
			}
			writer = new PrintWriter(file, "UTF-8");
			writer.println("index, time, x, y");
		} catch (Exception e) {
			Log.error(e);
		}
		
		while (camera.read(frame)) {
			proceedFrame(false, true, writer, dir);
		}
		
		writer.flush();
		writer.close();
	}
	
	private void proceedFrame(boolean show, boolean save, PrintWriter writer, File dir) {
		
		if(contours == null){
			contours = new ArrayList<MatOfPoint>();
		}
		
		Point point = new Point();
		
    	Imgproc.cvtColor(frame, hsv, Imgproc.COLOR_BGR2HSV);
    	
    	Core.inRange(hsv, new Scalar(40,0,200), new Scalar(150,155,255), mask);

    	Imgproc.findContours(mask, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
    	
    	MatOfPoint contour = null;

    	for(MatOfPoint item : contours){
    		if(contour == null || Imgproc.contourArea(item) > Imgproc.contourArea(contour)){
    			contour = item;
    		}
    	}
    	
    	if(contour == null){
    		return;
    	}
    	
    	Rect rect = Imgproc.boundingRect(contour);
    	
    	int dx = rect.width;
    	int dy = rect.height;
    	
    	rect.x = Math.max(0, rect.x - dx);
    	rect.y = Math.max(0, rect.y - dy);
    	
    	rect.width = Math.min(rect.width + dx * 2, mask.cols() - rect.x);
    	rect.height = Math.min(rect.height + dy * 2, mask.rows() - rect.y);
    	
    	Mat cropFrame = frame.submat(rect);
    	
    	Imgproc.cvtColor(cropFrame, cropFrame, Imgproc.COLOR_BGR2GRAY);
    	
    	if(filterMethod == FILTER_GAUSS){
    		Imgproc.GaussianBlur(cropFrame, cropFrame, new Size(blurSize, blurSize), 0, 0, Core.BORDER_DEFAULT);
    	} else {
    		Imgproc.medianBlur(cropFrame, cropFrame, blurSize);
    	} 
    	
    	
    	Imgproc.threshold(cropFrame, cropFrame, threshold, 0, Imgproc.THRESH_TOZERO);

    	Mat markedFrame = frame.clone();
    	
    	Imgproc.rectangle(markedFrame,
    			new Point(rect.x, rect.y), 
    			new Point(rect.x + rect.width, rect.y + rect.height),
    			new Scalar(255,0,255), 1);

    	double value;
    	double si = 0;
    	double sj = 0;
    	double s = 0;
    	for(int i=0;i<cropFrame.cols();i++){
    		for(int j=0;j<cropFrame.rows();j++){
    			value = cropFrame.get(j, i)[0];
    			si += (i+1) * value;
    			sj += (j+1) * value;
    			s += value;
        	}
    	}
    	
    	point.x = si/s;
    	point.y = sj/s;
    	
    	Point pointTarget;
    	
    	if(show){
        	Imgproc.cvtColor(cropFrame, cropFrame, Imgproc.COLOR_GRAY2BGR);
        	
        	pointTarget = new Point(point.x * (cropSize.width / cropFrame.cols()), point.y * (cropSize.height /  cropFrame.rows()));
        	
        	Imgproc.resize(cropFrame, cropFrame, cropSize, 0, 0, Imgproc.INTER_AREA);
        
        	Imgproc.circle(cropFrame, pointTarget, 2, markerColor, 2);
        	
        	markedFrame.get(0,0,((DataBufferByte) image.getRaster().getDataBuffer()).getData());
    		cropFrame.get(0,0,((DataBufferByte) crop.getRaster().getDataBuffer()).getData());
    		
    		label.update(label.getGraphics());
    		labelCrop.update(labelCrop.getGraphics());
    	}
    	
    	if(save){
    		
    		cropFrame.release();
    		cropFrame = frame.submat(rect);
    		
    		pointTarget = new Point(point.x * 10, point.y * 10);
    		
    		Imgproc.resize(cropFrame, cropFrame, new Size(cropFrame.cols() * 10, cropFrame.rows() * 10), 0, 0, Imgproc.INTER_AREA);
    		
    		Imgproc.circle(cropFrame, pointTarget, 2, markerColor, 2);
    		
    		saveFrame(cropFrame, new File(dir, + camera.get(App.CAP_PROP_POS_FRAMES) + ".jpg"));
    		
    		writer.println((int)camera.get(App.CAP_PROP_POS_FRAMES) + "," + camera.get(App.CAP_PROP_POS_MSEC) + "," + (point.x + rect.x) + "," + (point.y + rect.y));
    		
    	}
    	
    	contours.clear();

    	markedFrame.release();
    	cropFrame.release();
	}

	private void saveFrame(Mat frame, File file) {
		
		BufferedImage image = new BufferedImage(frame.cols(),frame.rows(), BufferedImage.TYPE_3BYTE_BGR);
		
		frame.get(0,0,((DataBufferByte) image.getRaster().getDataBuffer()).getData());
		
		ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
		ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
		jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		jpgWriteParam.setCompressionQuality(1f);
		
		try {
			jpgWriter.setOutput(new FileImageOutputStream(file));
			IIOImage outputImage = new IIOImage(image, null, null);
			jpgWriter.write(null, outputImage, jpgWriteParam);
		} catch (Exception e) {
			Log.error(e);
		}
		jpgWriter.dispose();
		
	}

	private File getOutputFrameDir() {
		File frameDir = new File(src + ".data", threshold+"_"+(filterMethod == FILTER_GAUSS ? "gauss" : "median")+"_"+blurSize).getAbsoluteFile();
		if(!frameDir.exists()){
			frameDir.mkdirs();
		} else {
			for(File file : frameDir.listFiles(new FilenameFilter(){
				@Override
				public boolean accept(File dir, String name) {
					return name.endsWith(".jpg");
				}})){
				file.delete();
			}
		}
		return frameDir;
	}

}
