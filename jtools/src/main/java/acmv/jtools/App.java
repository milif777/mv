package acmv.jtools;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencv.core.Core;

import acmv.jtools.laserbeam.EnergyCenter;


public class App 
{

	private static final Pattern regexParam = Pattern.compile("^-(.+)={0,1}(.*?)$");
	
	public static final int CAP_PROP_FRAME_COUNT = 7;
	public static final int CAP_PROP_FRAME_WIDTH = 3;
	public static final int CAP_PROP_FRAME_HEIGHT = 4;
	public static final int CAP_PROP_POS_FRAMES = 1;
	public static final int CAP_PROP_POS_MSEC = 0;

	private static Set<Runnable> task = new HashSet<Runnable>();

	public static void main( String[] args ) throws Exception
    {

		if(args.length == 0){
			return;
		}
		
		Map<String, String> params = new HashMap<String, String>();
		Matcher m;
		for(int i=0;i<args.length;i++){
			m = regexParam.matcher(args[i]);
			if(m.find()){
				params.put(m.group(1), m.group(2));
			}
		}
		
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );

		switch(args[0].toLowerCase()){
			case "laser.energycenter":
				beamEnergycenter(params, args[args.length - 1]);
				break;
		}

    	
    }

	private static void beamEnergycenter(Map<String, String> params, String src) {
		
		EnergyCenter ec = new EnergyCenter(src);
		task.add(ec);
		ec.run();
		
	}


}
