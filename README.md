## Jtools
### Requirements
- Java JRE 8
- [Opencv 3.2](http://opencv.org/releases.html) with Java support

### Quick start
`java -Djava.library.path=[PATH_TO]/opencv3/3.2.0/share/OpenCV/java -jar [PATH_TO]/jtools-***-all.jar laser.energycenter [PATH_TO_VIDEO]`